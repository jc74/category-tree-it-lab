import { Add, Remove } from "@mui/icons-material";
import { TreeItem } from "@mui/lab";
import { Box, Button } from "@mui/material";
import CategoryTreeItem from "./CategoryTreeItem";

const CategoryTreeContainer = ({ category, label, handleAddChild, handleRemoveChild }) => {

  return (
    <Box display="flex" alignItems="flex-start">
      <TreeItem nodeId={category.id.toString()} label={label}>
        {category.children && category.children.map((childCategory, key) => (
          <CategoryTreeItem
            key={childCategory.id}
            category={childCategory}
            label={label + "_" + ++key}
            handleAddChild={handleAddChild}
            handleRemoveChild={handleRemoveChild}
          />
        ))}
      </TreeItem>
      <Button onClick={() => handleAddChild(category.id)}><Add /></Button>
      {category.parent && (
        <Button onClick={() => handleRemoveChild(category.id)}><Remove /></Button>
      )}
    </Box>
  );
};

export default CategoryTreeContainer;