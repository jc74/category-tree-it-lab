import { ChevronRight, ExpandMore } from "@mui/icons-material";
import { TreeView } from "@mui/lab";
import { Button } from "@mui/material";
import { useState } from "react";
import { getRawTree, getTree, setDefaultData, setTree } from "../../utils/treeManager";
import CategoryTreeItem from "./CategoryTreeItem";

const CategoryTreeContainer = () => {
  const [categoryTree, setCategoryTree] = useState(getTree());

  const handleAddChild = (parentId) => {
    const rawTree = getRawTree();
    const lastElem = rawTree.at(-1);
    rawTree.push({ id: lastElem.id + 1, parent: parentId });
    setTree(rawTree);
    setCategoryTree(getTree());
  };

  const handleRemoveChild = (id) => {
    const rawTree = getRawTree();
    const deletedIds = getDeletedIds(rawTree, id);
    const updatedTree = rawTree.filter((item) => !deletedIds.includes(item.id));
    setTree(updatedTree);
    setCategoryTree(getTree());
  };

  const handleSetDefault = () => {
    setDefaultData();
    setCategoryTree(getTree());
  };

  const getDeletedIds = (tree, id) => {
    const deletedIds = [id];
    const children = tree.filter((item) => item.parent === id);

    for (const child of children) {
      deletedIds.push(...getDeletedIds(tree, child.id));
    }

    return deletedIds;
  };

  return (
    <>
      <TreeView
        defaultCollapseIcon={<ExpandMore />}
        defaultExpandIcon={<ChevronRight />}
        sx={{ overflowX: "hidden" }}
        key="root"
      >
        {categoryTree.map(rootCategory => (
          <CategoryTreeItem
            key={rootCategory.id}
            category={rootCategory}
            label={rootCategory.id.toString()}
            handleAddChild={handleAddChild}
            handleRemoveChild={handleRemoveChild}
          />
        ))}
      </TreeView>
      <Button onClick={() => handleSetDefault()}>Set default</Button>
    </>
  );
};

export default CategoryTreeContainer;