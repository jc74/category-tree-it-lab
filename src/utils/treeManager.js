const key = "categoryTree";

const defaultData = [
  {
    id: 1,
    parent: null
  },
  {
    id: 2,
    parent: null
  },
  {
    id: 3,
    parent: null
  },
  {
    id: 4,
    parent: 1
  },
  {
    id: 5,
    parent: 1
  },
  {
    id: 6,
    parent: 4
  },
  {
    id: 7,
    parent: 2
  }
];

const createTree = (data, parent = null) => {
  const tree = [];
  data.forEach(item => {
    if (item.parent === parent) {
      const children = createTree(data, item.id);
      if (children.length > 0) {
        item.children = children;
      }
      tree.push(item);
    }
  });
  return tree;
}

export const setDefaultData = () => {
  localStorage.setItem(key, JSON.stringify(defaultData));
};

export const setTree = (tree) => {
  localStorage.setItem(key, JSON.stringify(tree));
};

export const getTree = () => {
  return createTree(getRawTree());
};

export const getRawTree = () => {
  if (!localStorage.getItem(key)) {
    setDefaultData();
  }

  return JSON.parse(localStorage.getItem(key));
};