import CategoryTreeContainer from "./components/categoryTree/CategoryTreeContainer";
import "./App.css";

function App () {
  return (
    <>
      <CategoryTreeContainer />
    </>
  );
}

export default App;
